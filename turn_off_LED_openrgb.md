ды### Install

-   Download [Linux x64 .dev version-0.6](https://openrgb.org/releases.html)
-   Install dependencies

```
sudo apt install git build-essential qtcreator qtbase5-dev qtchooser qt5-qmake qtbase5-dev-tools libusb-1.0-0-dev libhidapi-dev pkgconf libmbedtls-dev qt5-default
```

-   Unpack

```
sudo dpkg -i open_xxx_amd64_xxxxxx.deb
```

### Create a preset

1. Run `openrgb`
2. Choose LED setup you need
3. Click "Save preset" and name it

### Run on startup

1. `mkdir ~/.config/autostart`
2. `nano ~/.config/autostart/OpenRGB.desktop`

```
   [Desktop Entry]
   Type=Application
   Encoding=UTF-8
   Name=OpenRGB
   Comment=Control RGB lighting
   Exec=openrgb -p $YOUR_PRESET_NAME
   Icon=OpenRGB
   Terminal=false
   Categories=Utility;
```
